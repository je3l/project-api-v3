import React from "react";

// import { motion } from "framer-motion"
// import {motion} from 'framer-motion/dist/es/index'
import {motion} from 'framer-motion/dist/framer-motion'

import "./Title.css";

export default function Title(props) {

  // varients for Framer motion Animations
  const variants = {
    rotating: {
      scale: [1, 2, 2, 1, 1],
      rotate: [0, 0, 270, 270, 0],
      borderRadius: ["20%", "20%", "50%", "50%", "20%"],
      duration: 2 
    },
    stable: { 
      rotate: [
        0, 20, 40, 20, 0, -20 ,-40, -20, 0,
        0, 20, 40, 20, 0, -20 ,-40, -20, 0,
        0, 20, 40, 20, 0, -20 ,-40, -20, 0,
        0, 20, 40, 20, 0, -20 ,-40, -20, 0,
        0, 20, 40, 20, 0, -20 ,-40, -20, 0
      ],
      // duration: 10
    }
  }

  return (
        <>
            <div className="lol-title">L
        <motion.div
            animate={props.animateEmoji ? 'rotating' : 'stable'}
            variants={variants}
        >
            <span role="img" aria-label="lol emoji">🤣</span>
        </motion.div>
        L</div>

        </>
    )
}
