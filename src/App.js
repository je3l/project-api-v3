import React, { useState, useEffect } from "react";

// Import and apply CSS stylesheet
import "./App.css";

// components
import Title from './Title.js'

// utils
import fetchReallyRandomJoke from './utils/fetchJokes'
import fetchRandomLaughingGIF from './utils/fetchGIF'
import backgroundLaughSound from './utils/bgSound'

// Home function that is reflected across the site
export default function Home() {
  const [joke, setJoke] = useState("Ready to laugn ???...")
  const [GIF, setGIF] = useState("https://giphy.com/embed/10JhviFuU2gWD6")
  const [animateEmoji, setAnimateEmoji] = useState(false)
  
  useEffect(() => { // fetch initially
    fetchReallyRandomJoke(setJoke)
    fetchRandomLaughingGIF(setGIF)
  }, [])

  useEffect(() => { // fetch on click changes
    backgroundLaughSound()
    fetchRandomLaughingGIF(setGIF)
    setAnimateEmoji(true)

    setTimeout(() => setAnimateEmoji(false), 3000);
  }, [joke])

  return (
    <div className="translate-middle">
      <Title animateEmoji={animateEmoji} />
      <h4 className="lol-joke">{joke}</h4>
      <iframe title="Laigh GIF" className="lol-gif" src={GIF} width="368" height="480" frameBorder="0" allowFullScreen></iframe>
      <button className="lol-button" onClick={() => fetchReallyRandomJoke(setJoke)}>laugh more worry less</button>
    </div>
  );
}
