import axios from 'axios'

const icanhazdadjoke = async (callback) => {
    const options = {
      method: 'GET',
      url: 'https://icanhazdadjoke.com/',
      headers: {
      Accept: 'text/plain'
      }
    };
  
    axios.request(options).then(function (response) {
      callback(response.data)
      // console.log('dad joke', response.data)
    }).catch(function (error) {
      callback("Where did the API go to eat? To the RESTaurant.")
      console.error(error);
    });
  }
  
  const fetchHumorJoke = async (callback) => {
    
    
     axios.get('https://backend-omega-seven.vercel.app/api/getjoke')
        .then(function (response) {
          
        // console.log(response.data[0].question)
      callback(response.data[0].question +"          "+ response.data[0].punchline)
          return response
        })
        .catch(function (error) {
          
          console.log(error);
        callback("Your Life. Yes YOUR Life.")
        })
  }
  
  const fetchChuckJoke = async (callback) => {
    axios.get('https://api.chucknorris.io/jokes/random')
        .then(function (response) {
          
        // console.log(response.data.value)
      callback(response.data.value)
          return response.data.value
        })
        .catch(function (error) {
          
          console.log(error);
        callback("Your Life. Yes YOUR Life.")
        })
  }
  
  const fetchDadJoke = async (callback) => {
    const options = {
      method: 'GET',
      url: 'https://dad-jokes.p.rapidapi.com/random/joke',
      headers: {
        'X-RapidAPI-Host': 'dad-jokes.p.rapidapi.com',
      'X-RapidAPI-Key': '1d84068d6amsh42618abf75e9490p1a8955jsn6a998614019b'
      }
    };
  
    axios.request(options).then(function (response) {
      callback(response.data.body[0].punchline +"          "+ response.data.body[0].setup)
      // console.log('dad joke', response)
    }).catch(function (error) {
      fetchChuckJoke(callback) // this API will work since it's with no call limits
      console.error(error);
    });
  }

  export default function fetchReallyRandomJoke(callback) {
    
    const random = Math.floor(Math.random() * 4)
    
    switch(random) {
      case 1:
        fetchChuckJoke(callback)
        break
      case 2:
        fetchHumorJoke(callback)
        break
      case 3:
        icanhazdadjoke(callback)
        break
      case 4:
        fetchDadJoke(callback)
        break
      default:
        callback("Now laugh.")
    }
  }
