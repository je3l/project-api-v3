import axios from 'axios'

export default function fetchRandomLaughingGIF(callback) {
 axios({
      method: 'GET',
      url: `https://api.giphy.com/v1/gifs/random?api_key=uzDQyn3AKhszwHFsYxo9y4hWOEVYWneO&tag=laughing`,
    }).then(res => callback(res.data.data.embed_url))
      .catch(error => console.log("gif error--> ",error))
  }