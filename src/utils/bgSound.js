export default function backgroundLaughSound() {
    const soundsURLs = [
      "https://cdn.glitch.global/8d8907b6-4f23-4f7f-a3ed-656000b2216f/0.mp3?v=1649918137619",
      "https://cdn.glitch.global/8d8907b6-4f23-4f7f-a3ed-656000b2216f/1.mp3?v=1649918142143",
      "https://cdn.glitch.global/8d8907b6-4f23-4f7f-a3ed-656000b2216f/2.mp3?v=1649918141590",
      "https://cdn.glitch.global/8d8907b6-4f23-4f7f-a3ed-656000b2216f/3.mp3?v=1649918141323",
      "https://cdn.glitch.global/8d8907b6-4f23-4f7f-a3ed-656000b2216f/4.mp3?v=1649918138504",
      "https://cdn.glitch.global/8d8907b6-4f23-4f7f-a3ed-656000b2216f/5.mp3?v=1649918138488",
      "https://cdn.glitch.global/8d8907b6-4f23-4f7f-a3ed-656000b2216f/6.mp3?v=1649918138105",
      "https://cdn.glitch.global/8d8907b6-4f23-4f7f-a3ed-656000b2216f/7.mp3?v=1649918141240",
      "https://cdn.glitch.global/8d8907b6-4f23-4f7f-a3ed-656000b2216f/8.mp3?v=1649918138052",
      "https://cdn.glitch.global/8d8907b6-4f23-4f7f-a3ed-656000b2216f/9.mp3?v=1649918138081",
      "https://cdn.glitch.global/8d8907b6-4f23-4f7f-a3ed-656000b2216f/10.mp3?v=1649918143154"
    ]
    
    let audio = new Audio(soundsURLs[Math.floor(Math.random() * soundsURLs.length)]);
    audio.play();
  }