## Assignment Instructions

- [x] Pick a unique API to get some data and display it on your new webpage
- [x] Pick a unique animation from [popmotion](https://popmotion.io/pure/) (Links to an external site.) and display its functionality on your webpage 
    - Popmotion is deprecated by Framer Motion. library so now i'm using that one now (https://popmotion.io/pose/)
- Link to older working library [Links to an external site.](https://unpkg.com/popmotion@8.1.24/dist/popmotion.global.min.js) of popmotion
- [x] Make use of the advanced CSS properties we discussed to better design your webpage ( flexbox, grid, bootstrap, etc)
- [ ] Sign Up sheet will be up on canvas

## Figma Quick Design
![Figma Quick Design Image](https://cdn.glitch.global/8d8907b6-4f23-4f7f-a3ed-656000b2216f/design.png?v=1650167625532)
