# project-API-v3

Demo. Of APIs in React

## Deployed at

https://je3l.gitlab.io/project-api-v3

## About

* Implemented in React.js and uses 'https://www.npmjs.com/package/axios' to send API requests.
* This a project which uses ['https://icanhazdadjoke.com/', 'https://api.chucknorris.io/', 'https://rapidapi.com/KegenGuyll/api/dad-jokes' and 'https://backend-omega-seven.vercel.app/api/getjoke'] APIs, selects one of them randomnly and fetched random joke from that API.
* Also it uses https://giphy.com/ and fetched random Laughing GIF everytime a new joke is fetched.
* Since Popmotion is deprecated (https://popmotion.io/pose/) and replaced by Framer Motion library, so now i'm using that one now.

# Implementation Details

## Provide link to API used
- ['https://icanhazdadjoke.com/', 'https://api.chucknorris.io/', 'https://rapidapi.com/KegenGuyll/api/dad-jokes' and 'https://backend-omega-seven.vercel.app/api/getjoke']

## Provide link to Animation used
- Something like this --> https://www.framer.com/docs/examples/#keyframes

## During presentation be able to explain the documentation used in your API to the class. 
- 

## How were you fetching for the data?
- Using Axios library

## What type of data gets returned? 
- JSON and Text

## How does it look in JSON format. 
- Objects like structured strings

## Properties used from the JSON object.
- parse

## Be able to explain what Popmotion variables were used and how they work in your app. 
- 

## Issues encountered? (CORS?) how were they resolved. 
- Earlier when i implemented this app only using HTML5 and CSS3, but later i switched to React and didn't faced any CORS.
- But the popmotion module for React was depricated and replaced by Framer motion, so i used that one, as per suggestion by popmotion docs.

